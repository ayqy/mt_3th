(function() {
    window.my = window.my || {};
    my.Test = Test;

    // 性能测试
    /**
     * 运行测试用例
     * @param  {Number} count 数据量
     * @param  {Number} len   可选，串长，控制数据重复率
     * @return
     */
    function Test(count, len) {
        if (!my.fun1 || !my.fun2) {
            new Error('solution.js is not found');
        }

        var arr = _genData(count, len);

        // 测试方法1  后去重
        _run(function() {
            my.fun1(arr);
        }, '方法1_后去重');
        // 测试方法2  先去重
        _run(function() {
            my.fun2(arr);
        }, '方法2_先去重');
    }
    /**
     * 生成测试数据
     * @param  {Number} count 数据量
     * @param  {Number} len 可选，串长，控制数据重复率
     * @return {[type]}            [description]
     */
    function _genData(count, len) {
        var origin = 'abcdefghijklmnopqrstuvwxyz0123456789';
        var arr = [];

        function rand035() {
            return parseInt(Math.random() * 36, 10);
        }

        for (var i = 0; i < count; i++) {
            var length = (len - 1) || (rand035() + 1);
            arr[i] = '';

            for (var j = 0; j <= length; j++) {
                arr[i] += origin.charAt(rand035());
            }
// console.log(arr[i]);
        }

        return arr;
    }

    // 计时器
    function _run(func, strTimeId) {
        console.time('#start_' + strTimeId);

        func();

        console.timeEnd('#start_' + strTimeId);
    }
    // run(function() {alert(1);}, 'alert');
})();