(function() {
    window.my = window.my || {};
    my.fun1 = fun1;
    my.fun2 = fun2;


    // 方法1（后去重）
    function fun1(array){
    console.log('方法1（后去重）');
        // 复制
        var arr = array.slice();
        var tmpArr = array.slice();

        // 内部排序
        for (var i = 0; i < arr.length; i++) {
            arr[i] = arr[i].split('').sort().join('');
        }

        // 筛选
        var res = [];
        var hash = [];  // 暂存已经被记录的元素
        for (var i = 0; i < arr.length; i++) {
            var find = false;
            for (var j = i + 1; j < arr.length; j++) {
                if (arr[i] === arr[j] && !hash[i]) {    // 组成相同 & 没有被记录过
                    if (!find) {
                        res[res.length] = [];
                    }
                    res[res.length - 1].push(j);    // 记录后面的索引
                    hash[j] = true; // j被记录过了
                    find = true;
                }
            }
            if (find) {
                res[res.length - 1].unshift(i);    // 记录第一个索引
                hash[i] = true; // i被记录过了
            }
        }

        // 去重
        var result = [];
        for (var i = 0; i < res.length; i++) {
            result[result.length] = [];
            for (var j = 0; j < res[i].length; j++) {
                if (result[result.length - 1].indexOf(tmpArr[res[i][j]]) === -1) {
                    result[result.length - 1].push(tmpArr[res[i][j]]);
                }
            }
        }

        return result;
    }


    // 方法2（先去重）
    function fun2(array){
    console.log('方法2（先去重）');
        // 复制
        var arr = array.slice();
        var hash = {};

        // 去重
        var arrTmp = [];
        for (var i = 0; i < arr.length; i++) {
            if (!hash[arr[i]]) { // 不重复
                arrTmp.push(arr[i]);
                hash[arr[i]] = true;
            }
        }
        var uniqueArr = arrTmp.slice();
        arr = arrTmp;

        // 内部排序
        for (var i = 0; i < arr.length; i++) {
            arr[i] = arr[i].split('').sort().join('');
        }

        // 筛选 & 构造结果集
        var result = [];
        for (var i = 0; i < arr.length; i++) {
            var find = false;
            for (var j = i + 1; j < arr.length; j++) {
                if (arr[i] === arr[j]) {    // 组成相同
                    if (!find) {
                        result[result.length] = [];
                    }
                    result[result.length - 1].push(uniqueArr[j]);   // 记录后面的值
                    find = true;
                }
            }
            if (find) {
                result[result.length - 1].unshift(uniqueArr[i]);    // 记录第一个值
            }
        }
    // console.table(result);///

        return result;
    }
})();